from pytube import YouTube, Playlist
import argparse

path = "/home/hank/Downloads"


def single_video():
    vid = YouTube(video).streams.first().download(path)
    name = vid.title()
    print(f"{name} has downloaded.")


def whole_playlist():
    pl = Playlist(playlist).download_all(path)
    print("Playlist has finished downloading.")


parser = argparse.ArgumentParser()
parser.add_argument(
    "-v", action="store", dest="Video", help="Single video YouTube link", type=str
)
parser.add_argument(
    "-p", action="store", dest="Playlist", help="Playlist YouTube link", type=str
)
args = parser.parse_args()

video = args.Video
playlist = args.Playlist

if video == None and playlist == None:
    print("It seems as though you did not enter a valid YouTube link.")

elif video != None:
    single_video()

elif playlist != None:
    whole_playlist()
