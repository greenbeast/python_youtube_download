# Python YouTube Downloader

Basic YouTube video downloader written in Python using `pytube`. There is a CLI version which is `youtube_python_cli.py` and a GUI version. To launch the GUI you can just `cd` into the `dist` folder and launch `youtube_python_gui` and it should launch the UI.
#### Examples for CLI
To download a single video go to YouTube and copy your link. `cd` into the directory of this script then run the following command:

`python3 youtube_python_cli.py -v 'your-URL'` and replace 'your-URL' with your URL (duh). Make sure it is in the quotations or else it won't work.

To download a playlist go to YouTube and copy your link and `cd` into the directory and run this script with the following command:

`python3 youtube_python_cli.py -p 'your-URL'` and replace 'your-URL' with your URL (duh). Make sure it is in the quotations or else it won't work.

#### GUI
There is currently a very basic GUI `youtube_python_gui.py`. So just `cd` into the directory of the script then run it with `python3 youtube_python_gui.py` and it'll launch a window(make sure tkinter is installed). From there you'll see a textbox to put a URL into and two buttons, one for a single video and another for a playlist. Both are labeled for clarity.

Instead of launching it through the terminal you can also launch the GUI from a file manager by going into the `dist` folder after cloning the repo and just clicking on `youtube_python_gui`.