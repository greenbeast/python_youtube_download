import tkinter.messagebox
from tkinter import *
from tkinter import filedialog, scrolledtext
from pytube import YouTube, Playlist

path = "/home/hank/Downloads"

# https://www.youtube.com/watch?v=9ORXZA2k2OA

# -------------- Functions for downloading --------------
def single_video():
    try:
        input = url.get()
        vid = YouTube(input).streams.first().download(path)
        name = vid.title()
        print(f"{name} has downloaded.")
        url.delete(0, END)
        tkinter.messagebox.showinfo("YouTube Download", f"{name} has been downloaded")
    except:
        tkinter.messagebox.showinfo(
            "YouTube Download",
            "Unable to convert the video. Make sure you are connected to the internet and the URL is valid.",
        )


def whole_playlist():
    try:
        input = url.get()
        pl = Playlist(input).download_all(path)
        print("Playlist has finished downloading.")
        url.delete(0, END)
        tkinter.messagebox.showinfo(
            "YouTube Download", f"Playlist has been downloaded at {path}"
        )
    except:
        tkinter.messagebox.showinfo(
            "YouTube Download",
            "Unable to convert the playlist. Make sure you are connected to the internet and the URL is valid.",
        )


# ------------------ GUI code ------------------

root = Tk()
root.title("YouTube Downloader")
root.geometry("250x150")
url = Entry(root, width=30)
url.pack()

submit_single = Button(root, text="Submit URL(single video)", command=single_video)
submit_single.pack()

submit_play = Button(root, text="Submit URL(playlist)", command=whole_playlist)
submit_play.pack()


root.mainloop()
